## Lottie Simple Editor
A simple Lottie real-time editor, developed with React and CRDT.

## Demo

![screencast](https://i.imgur.com/90ZC0RO.gif)


## Development

```
docker-compose up -d
```

For development, the `server/` and `client/` directories have their own docker containers, which are configured via the `docker-compose.yml` file.

The client server is spun up at `localhost:3000` and it proxies internally to the server using the linked name as `localhost:5000`, and relay server as `localhost:8081`.

After deployed, visit http://localhost:3000 for development app.

## Production

```
docker-compose -f docker-compose-prod.yml up -d
```

For production, this uses the `Dockerfile.client.prod` and `Dockerfile.server.prod` at the root of this repo. React app deploys to nginx server to serve static react files, and API server and relay servers deploy with docker containers.

After deployed, visit http://localhost:5000 for production app.

## Environment Variables

### Client

All environment variables can be found and edit in `docker-compose.yml (dev)`  and `docker-compose-prod.yml (prod)`.

| Variable              | Default               |                                       |
| --------------------- | --------------------- | --------------------------------------|
| `REACT_APP_BASE_API`  | `localhost:5000`      | Connect to backend API.               |
| `REACT_APP_RELAYS`    | `ws:localhost:8080`   | Connect to one or more relay servers. |

### Server

All environment variables can be found and edit in `docker-compose.yml (dev)` and `docker-compose-prod.yml (prod)`.

| Variable              | Default               |                                     |
| --------------------- | --------------------- | ----------------------------------- |
| `NODE_ENV`            | `development`         | Deployment type.                    |
| `ENABLE_RELAY_SERVER` | `false`               | Deploy server as relay or express.  |



## CRDT References

`@localfirst/state` is an automatically replicated Redux store that gives your app offline capabilities and secure peer-to-peer synchronization superpowers.

- Data replication & synchronization, using the `Automerge` library
- Persistence to a local or remote data store. You can use the provided adapters for `IndexedDb` or `MongoDb`, or provide your own.

🡒 [Read more](https://github.com/local-first-web/state)



`@localfirst/relay` is a tiny service that helps local-first applications connect with peers on other devices. It can run in the cloud or on any device with a known address.

🡒 [Read more](https://github.com/local-first-web/relay)





