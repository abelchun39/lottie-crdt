## Lottie Simple Editor
ExpressJS API and Relay servers.



## Development

```
npm i
npm start
```

## Production

```
npm i
npm run build
npm run start:prod
```

## Environment Variables

All environment variables can be included in `.env` file in this folder.

| Variable              | Default               |                                     |
| --------------------- | --------------------- | ----------------------------------- |
| `NODE_ENV`            | `development`         | Deployment type.                    |
| `ENABLE_RELAY_SERVER` | `false`               | Deploy server as relay or express.  |



## CRDT Reference

`@localfirst/relay` is a tiny service that helps local-first applications connect with peers on other devices. It can run in the cloud or on any device with a known address.

🡒 [Read more](https://github.com/local-first-web/relay)





