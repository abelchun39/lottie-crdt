// Set options as a parameter, environment variable, or rc file.
require = require('esm')(module/*, options */)

require('module-alias/register')
require('dotenv').config({path: __dirname + '/../.env' })

module.exports = require('./index.js')