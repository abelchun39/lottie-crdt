import { Router } from 'express'
import demoData from './../demo/sample_lottie'

export default () => {
	let api = Router()
	/**********************/
	/** Public Endpoint **/
	/**********************/

    api.get('/lottie', (req, res) => {
        res.json(demoData)
    });

    // Otherwise
    api.get('/', (req, res) => {
        res.json({ message: "Welcome to Lottie API!" });
    });

	return api;
}
