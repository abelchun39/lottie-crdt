import startRelayServer from './relayServer'
import expressServer from './expressServer'

async function init () {
  try {
    if (process.env.ENABLE_RELAY_SERVER === "true") {
      console.log('start relay server')
      await startRelayServer()
    }else{
      console.log('start express server')
      await expressServer()
    }
  } catch (e) {
    console.log(e)
    process.exit(0)
  }
}

init()
