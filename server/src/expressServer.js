import http from 'http';
import express from 'express';
import cors from 'cors';
import morgan from 'morgan';
import bodyParser from 'body-parser';
import cookieParser from 'cookie-parser'
import api from './api';

export default async () => {
  let app = express();
  app.server = http.createServer(app)

  // cookie parser
  app.use(cookieParser())

  // logger
  app.use(morgan('dev'))

  // 3rd party middleware
  app.use(cors());

  // parse application/x-www-form-urlencoded
  app.use(bodyParser.urlencoded({ extended: false }))

  app.use(bodyParser.json({
    limit : "100mb"
  }));

  // api router
  app.use('/api', api());

  app.server.listen(process.env.PORT || 8080, () => {
    console.log(`Started on port ${app.server.address().port}`)
  })
}