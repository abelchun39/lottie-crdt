const { Server } = require('@localfirst/relay/dist')
const DEFAULT_PORT = 8080

export default async () => {
    const port = Number(process.env.PORT) || DEFAULT_PORT
    const server = new Server({ port })
    server.listen()
}
