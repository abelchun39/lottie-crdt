## Lottie Simple Editor
A simple Lottie real-time editor, developed with React and CRDT.

## Demo

![screencast](https://i.imgur.com/90ZC0RO.gif)


## Development

```
npm i
npm start
```

## Production

```
npm i
npm run build
npm install -g serve
serve -s build
```

## Environment Variables

All environment variables can be included in `.env` file in this folder.

| Variable              | Default               |                                       |
| --------------------- | --------------------- | --------------------------------------|
| `REACT_APP_BASE_API`  | `localhost:5000`      | Connect to backend API.               |
| `REACT_APP_RELAYS`    | `ws:localhost:8080`   | Connect to one or more relay servers. |


## CRDT Reference

`@localfirst/state` is an automatically replicated Redux store that gives your app offline capabilities and secure peer-to-peer synchronization superpowers.

- Data replication & synchronization, using the `Automerge` library
- Persistence to a local or remote data store. You can use the provided adapters for `IndexedDb` or `MongoDb`, or provide your own.

🡒 [Read more](https://github.com/local-first-web/state)
