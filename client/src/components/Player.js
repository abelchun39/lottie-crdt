import React, { useRef } from 'react';
import _ from 'lodash'
import { Player, Controls } from '@lottiefiles/react-lottie-player';
import { makeStyles } from '@material-ui/core/styles';
import { useGlobalState } from "@morefaie/react-useglobalstate";
import { DRAWER_WIDTH } from '../utils/constant';

const useStyles = makeStyles(() => ({
    playerContainer: {
        width: `calc(100% - ${(DRAWER_WIDTH*2) - 20}px)`,
        margin: "0 auto"
    }
}));

export const LottiePlayer = () => {
    const classes = useStyles();
    const [lottieJSON,] = useGlobalState("lottie");
    const playerRef = useRef();

    const removeEmpty = (obj) => {
        let finalObj = {};
        Object.keys(obj).forEach((key) => {
            if (obj[key] && typeof obj[key] === 'object') {
                const nestedObj = removeEmpty(obj[key]);
                if (Object.keys(nestedObj).length) {
                    finalObj[key] = nestedObj;
                }
            } else if (obj[key] !== '' && obj[key] !== undefined && obj[key] !== null) {
                finalObj[key] = obj[key];
            }
        });
        return finalObj;
    }

    return (
        <div className={classes.playerContainer}>
            {lottieJSON && Object.keys(lottieJSON).length > 0 && playerRef && (
                <>
                    <Player
                        ref={playerRef}
                        loop
                        src={lottieJSON}
                        style={{ height: '90vh', width: '500px' }}
                    >
                        <Controls visible={true} buttons={['play', 'repeat', 'frame', 'debug']} />
                    </Player>
                </>
            )}
        </div>
    );
}