import _ from 'lodash'
import React, { useEffect, useCallback } from 'react';
import Lottie from 'react-lottie-player';
import { makeStyles } from '@material-ui/core/styles';
import Drawer from '@material-ui/core/Drawer';
import Toolbar from '@material-ui/core/Toolbar';
import InputLabel from '@material-ui/core/InputLabel';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemAvatar from '@material-ui/core/ListItemAvatar';
import ListItemText from '@material-ui/core/ListItemText';
import { useGlobalState } from "@morefaie/react-useglobalstate";
import { getLottieFile } from './../api/lottie';
import { addLayerColorsToLottie } from './../utils/helper'
import { DRAWER_WIDTH } from '../utils/constant';

const useStyles = makeStyles((theme) => ({
    drawer: {
        width: DRAWER_WIDTH,
        flexShrink: 0,
    },
    drawerPaper: {
        width: DRAWER_WIDTH,
        backgroundColor: "#e7e7e7"
    },
    drawerContainer: {
        overflow: 'auto',
    },
    drawerSectionLayers: {
        padding: "1em 0em"
    },
    drawerTitleLayers: {
        paddingLeft: "1em",
        paddingRight: "1em",
    }
}));



export const LeftDrawer = () => {
    const classes = useStyles();
    const [, setLottie] = useGlobalState("lottie");
    const [, setSelectedLayerIndex] = useGlobalState("selectedLayerIndex")
    const [layers, setLayers] = useGlobalState("layers")
    const [,setIsLoading] = useGlobalState("isLoading")

    useEffect(() => {
        (async () => {
            let _lottieJSON
            try {
                let result = await getLottieFile();
                let data = result.data;
                if(data) _lottieJSON = _.cloneDeep(data)
            } catch (e) {
                console.log(e);
            }
            if(!_lottieJSON) return;
            setLottie(_lottieJSON)
           
            if(_lottieJSON && _lottieJSON["layers"] && _lottieJSON["layers"].length > 0) {
                let _layerColors = addLayerColorsToLottie({ _lottieJSON })
                let decompLayers = _lottieJSON["layers"].map((l,lIndex) => {
                    return {
                        "id": lIndex,
                        "v":"5.5.1",
                        "fr":60,
                        "ip":0,
                        "op":180,
                        "w":500,
                        "h":500,
                        "nm": l["nm"],
                        "ddd":0,
                        "assets": [],
                        "chars": [],
                        "markers": [],
                        ...(_layerColors && _layerColors.length > 0 && _layerColors[lIndex] ? 
                            { layerColors: _layerColors[lIndex]["colors"] } : {}
                        ),
                        ...{ "layers": [l] }
                    }
                })

                if(decompLayers && decompLayers.length > 0) {
                    decompLayers[0]["selected"] = true
                    setLayers([...decompLayers])
                }
            }
            setIsLoading(false)
        })()
    },[])

    const handleLayerClick = useCallback(updatedLayer => {
        setLayers(prevLayers => {
            const prevLayerIndex = prevLayers.findIndex(lay => lay.selected === true)
            if(prevLayerIndex > -1) prevLayers[prevLayerIndex]["selected"] = false
            const layerIndex = prevLayers.findIndex(lay => lay.id === updatedLayer["id"])
            prevLayers[layerIndex] = updatedLayer
            setSelectedLayerIndex(layerIndex)
            return [ ...prevLayers ]
        })
    }, [])

    const Layer = React.memo(LayerComp)
    function LayerComp ({ layer, onUpdate }) {
        const _layerClick = d => onUpdate({ ...layer, selected: true })
        return (
            <ListItem
                button
                selected={layer["selected"] === true}
                onClick={(e) => _layerClick()}
            >  
                <ListItemAvatar>
                    <Lottie
                        loop
                        animationData={layer}
                        play
                        style={{ width: 50, height: 50 }}
                    />
                </ListItemAvatar>
                <ListItemText primary={`${layer["nm"]}`} />
            </ListItem>
        )
    }

    return (
        <Drawer
            className={classes.drawer}
            variant="permanent"
            classes={{
                paper: classes.drawerPaper,
            }}
            >
            <Toolbar />
            <div className={classes.drawerContainer}>
                <div className={classes.drawerSectionLayers}>
                    <InputLabel className={classes.drawerTitleLayers} >LAYERS</InputLabel>
                    <List component="nav" aria-label="main mailbox folders">
                        {
                            layers.map(layer => {
                                return (
                                    <Layer
                                        key={layer["id"]}
                                        layer={layer}
                                        onUpdate={handleLayerClick}
                                    />
                                )
                            })
                        }
                    </List>
                </div>
            </div>
        </Drawer>
    );
}