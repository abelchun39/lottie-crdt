import React, { useEffect, useState } from 'react';
import _ from 'lodash'
import useClippy from 'use-clippy';
import { snackbarService } from 'uno-material-ui';
import { makeStyles } from '@material-ui/core/styles';
import {
  AppBar,
  Toolbar,
  Typography,
  InputBase,
  Paper
} from "@material-ui/core";

import IconButton from '@material-ui/core/IconButton';
import FileCopyIcon from '@material-ui/icons/FileCopy';

const useStyles = makeStyles(() => ({
  grow: {
    flexGrow: 1,
  },
  title: {
    display: 'block',
    color: "white"
  },
  alignItemsAndJustifyContent: {
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'space-between',
  },
  appBar: {
    zIndex: 1300,
    backdropFilter: "blur(42px)"
  },
  iconButton: {
    padding: 10,
  },
  colabContainer: {
    display: "flex",
    alignItems: "center"
  },
  colabButtonContainer: {
    width: "360px"
  },
  colabTitle: {
    marginRight: "10px"
  },
  colabLinkField: {
    width: 'calc(100% - 44px)'
  }
}));

export default function PrimarySearchAppBar() {
  const classes = useStyles();
  const [colabLink, setColabLink] = useState("")
  const [, setClipboard] = useClippy();
  useEffect(() => {
    if(window && window.location) setColabLink(window.location.href)
  },[])

  const handleClipboard = () => {
    setClipboard(colabLink)
    snackbarService.showSnackbar(`Collaboration link had been copied!`, 'success')
  }
  
  return (
    <div className={classes.grow}>
      <AppBar color='secondary' className={classes.appBar}>
        <Toolbar className={classes.alignItemsAndJustifyContent}>
          <Typography className={classes.title} variant="h6">
            Lottie Simple Editor
          </Typography>

          <div className={classes.colabContainer}>
            <Typography className={classes.colabTitle} variant="h6">
                Collaboration Link
            </Typography>
            <Paper component="form" className={classes.colabButtonContainer}>
              
              <IconButton onClick={() => handleClipboard() } className={classes.iconButton} aria-label="menu">
                <FileCopyIcon />
              </IconButton>
              <InputBase
                readOnly={true}
                fullWidth={false}
                value={colabLink}
                className={classes.colabLinkField}
                placeholder="collaboration link"
                inputProps={{ 'aria-label': 'collaboration link' }}
              />
            </Paper>
          </div>
        </Toolbar>
      </AppBar>
    </div>
  );
}