import React, { useState, useEffect, useCallback } from 'react';
import _ from 'lodash'
import dot from "dot-object"
import { makeStyles } from '@material-ui/core/styles';
import Drawer from '@material-ui/core/Drawer';
import Toolbar from '@material-ui/core/Toolbar';
import InputLabel from '@material-ui/core/InputLabel';
import { ColorPicker } from 'material-ui-color';
import { useSelector, useDispatch } from "react-redux";
import { useGlobalState } from "@morefaie/react-useglobalstate";
import { updateLayerColorsFromLottie } from './../utils/helper'
import { DRAWER_WIDTH } from '../utils/constant';
import Actions from "./../store/actions";
import { getActions } from './../store/selectors'

const { manageAction } = Actions

const paletteObj = {
    red: '#ff0000',
    blue: '#0000ff',
    green: '#00ff00',
    yellow: 'yellow',
    cyan: 'cyan',
    lime: 'lime',
    gray: 'gray',
    orange: 'orange',
    purple: 'purple',
    black: 'black',
    white: 'white',
    pink: 'pink',
    darkblue: 'darkblue',
};
  

const useStyles = makeStyles((theme) => ({
    drawer: {
        width: DRAWER_WIDTH,
        flexShrink: 0,
    },
    drawerPaper: {
        width: DRAWER_WIDTH,
        backgroundColor: "#e7e7e7"
    },
    drawerContainer: {
        overflow: 'auto',
        padding: "0em 1em"
    },
    drawerSection: {
        paddingTop: "1em",
        paddingBottom: "1em",
    },
    drawerTitle: {
        marginBottom: "1em"
    }
}));

export const RightDrawer = () => {
    const classes = useStyles();
    const dispatch = useDispatch();
    const [lottie, setLottie] = useGlobalState("lottie");
    const [selectedLayerIndex, setSelectedLayerIndex] = useGlobalState("selectedLayerIndex");
    const [layers, setLayers] = useGlobalState("layers");
    const [colors, setColors] = useState([])
    const [isLoading,] = useGlobalState("isLoading")
    let actions = useSelector(getActions)

    useEffect(() => {
        /*******************
         * ACTION PROCESSOR
        ********************/
       
       if(!lottie || Object.keys(lottie).length === 0) return;
       let _actions = _.cloneDeep(actions)
       if(_actions && _actions.length > 0) {
           let _lottie = _.cloneDeep(lottie)
           let _colorChanged = []
           _actions.map(action => {
               if (action["type"] === "UPDATE_COLOR") {
                   let _updatedColorJSON = action["updatedData"]
                   let layerIndex = action["layerIndex"]
                   _colorChanged.push({ layerIndex, colorIndex: action["colorIndex"], colorHex: action["colorHex"], updatedColorJSON: _updatedColorJSON })
                   if(_updatedColorJSON) {
                       for(let [updatedKey, updatedVal] of Object.entries(_updatedColorJSON)) {
                           const obj = {}
                           obj[updatedKey] = updatedVal
                           dot.object(obj);
                           dot.copy(updatedKey, `layers[${layerIndex}].${updatedKey}`, obj, _lottie);
                       }
                   }
               }
           })

           // UI updated
           _colorChanged = _colorChanged.filter(e => e["layerIndex"] === selectedLayerIndex);
           if (_colorChanged && _colorChanged.length > 0) {
               setColors(prevColors => {
                   if(prevColors && prevColors.length > 0)
                       _colorChanged.map(c => prevColors[c["colorIndex"]]["v"] = c["colorHex"])
                   return [ ...prevColors ]
               })

               let _updatedLayers = _.cloneDeep([])
               _colorChanged.map(c => {
                   _updatedLayers = updateLayerColorsFromLottie({
                       layers,
                       layerIndex: c["layerIndex"],
                       colorIndex: c["colorIndex"],
                       colorHex: c["colorHex"],
                       updatedColorJSON: c["updatedColorJSON"]
                   })
               })
               setLayers([..._updatedLayers])
           }
           setLottie(_lottie)
       }
    }, [actions, !isLoading])

    useEffect(() => {
        let selectedLayer = (layers.length > 0) ? layers[selectedLayerIndex] : null;
        if(selectedLayer) {
            dot.object(selectedLayer)
            if(selectedLayer["layerColors"] && selectedLayer["layerColors"].length > 0) {
                let layerColors = _.cloneDeep(selectedLayer["layerColors"]);
                setColors([...layerColors])
            }
        }
    }, [selectedLayerIndex, layers && layers.length > 0])

    const Color = React.memo(ColorComp)
    function ColorComp ({ colorIndex, color, onUpdate }) {
        const _colorClick = (v) => {
            onUpdate({ colorIndex, ...v })
        };
        return (
            <div>
                <ColorPicker value={color} deferred palette={paletteObj} onChange={_colorClick}/>
            </div>
        )
    }

    const handleColorClick = useCallback(updatedColor => {
        let colorIndex = updatedColor["colorIndex"];
        let colorHex = `#${updatedColor["hex"]}`
        let layerIndex = selectedLayerIndex;
        let currentColor = colors[colorIndex];
        let updatedRgba = [
            parseFloat(updatedColor["rgb"][0]/255),
            parseFloat(updatedColor["rgb"][1]/255),
            parseFloat(updatedColor["rgb"][2]/255),
            updatedColor["alpha"]
        ]
        let updatedColorJSON;
        if(currentColor && currentColor["k"] && currentColor["k"].length > 0) {
            updatedColorJSON = currentColor["k"].map((c,cIndex) => {
                c["v"] = updatedRgba[cIndex];
                return c
            })
        }
        let _updatedColorJSON = updatedColorJSON.reduce((o,c,i) => {
            o[`${c['k'].toString()}`] = c["v"]
            return o;
        }, {})
        
        let updatedLottie = _.cloneDeep(lottie)
        if(updatedLottie && updatedLottie["layers"] && updatedLottie["layers"].length > 0) {
            if(_updatedColorJSON) {
                for(let [updatedKey, updatedVal] of Object.entries(_updatedColorJSON)) {
                    const obj = {}
                    obj[updatedKey] = updatedVal
                    dot.object(obj);
                    dot.copy(updatedKey, `layers[${layerIndex}].${updatedKey}`, obj, updatedLottie);
                }
            }
            setLottie(updatedLottie)
            let updatedLayers = updateLayerColorsFromLottie({
                updatedColorJSON: _updatedColorJSON,
                layers,
                layerIndex,
                colorIndex,
                colorHex
            })
            setLayers([...updatedLayers])

            dispatch(
                manageAction({
                    type: "UPDATE_COLOR",
                    updatedData: _updatedColorJSON,
                    layerIndex,
                    colorIndex,
                    colorHex
                })
            )
        }

        setColors(prevColors => {
            prevColors[colorIndex]["v"] = colorHex
            return [ ...prevColors ]
        })
        
    }, [colors, lottie, selectedLayerIndex, layers])

    return (
        <Drawer
            className={classes.drawer}
            variant="permanent"
            classes={{
                paper: classes.drawerPaper,
            }}
            anchor="right"
        >
            <Toolbar />
            <div className={classes.drawerContainer}>
                <div className={classes.drawerSection}>
                    <InputLabel className={classes.drawerTitle} htmlFor="lottieURL">COLORS</InputLabel>
                    {
                        colors.map((color,colorIndex) => {
                            return (
                                <Color
                                    key={colorIndex}
                                    colorIndex={colorIndex}
                                    color={color["v"]}
                                    onUpdate={handleColorClick}
                                />
                            )
                        })
                    }
                </div>
            </div>
        </Drawer>
    );
}
