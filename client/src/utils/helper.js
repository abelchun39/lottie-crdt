import _ from 'lodash'
import dot from 'dot-object'
import rgb2hex from 'rgb2hex';

export const getDayByDay = (data) => {
    const days = new Set([])
    return data.reduce((a, d, i) => {
        const day = new Date(d.dt * 1000).getDay();
        if(!days.has(day)) {
            a.push(d)
            days.add(day)
        }
        return a
    }, [])
}

export const addLayerColorsToLottie = ({ _lottieJSON }) => {
    let _layerColors = []
    _lottieJSON["layers"].map((l,i) => {
        if(l && l["shapes"] && l["shapes"].length > 0) {
            const shapes = dot.dot(l["shapes"]);
            let _tempColors = []
            for(let [k,v] of Object.entries(shapes)) {
                let isColor = k.indexOf(`c.k`);
                if(isColor > -1) {
                    _tempColors.push({ k: `shapes${k}`, v})
                }
            }
            let rgbaColors = _.chunk(_tempColors,4);
            if(rgbaColors && rgbaColors.length > 0){
                let layerColors = rgbaColors.reduce((a,arr,i) => {
                    let _c = arr.map(_ => _["v"])
                    let _hexColor = rgb2hex(`rgba(
                        ${parseInt(_c[0] * 255)},
                        ${parseInt(_c[1] * 255)},
                        ${parseInt(_c[2] * 255)},
                        ${_c[3]}
                    )`)
                    if(_hexColor && _hexColor["hex"]) {
                        a.push({
                            k: rgbaColors[i],
                            v: _hexColor["hex"].toUpperCase()
                        });
                    }
                    return a;
                }, [])                
                _layerColors.push({
                    "name": l["nm"],
                    "colors": layerColors
                })
            }
        }
    })
    return _layerColors;
}

export const updateLayerColorsFromLottie = ({
    layerIndex,
    colorIndex,
    colorHex,
    layers,
    updatedColorJSON
}) => {
    let _updatedLayers = { data: [] }
    if(layers && layers.length > 0) {
        _updatedLayers["data"] = _.cloneDeep(layers)
        let updatedColorList = Object.entries(updatedColorJSON)
        for(let [updatedKey, updatedVal] of updatedColorList) {
            const obj = {}
            obj[updatedKey] = updatedVal

            dot.object(obj);
            dot.copy(updatedKey, `data[${layerIndex}].layers[0].${updatedKey}`, obj, _updatedLayers);
        }
        let layerColors = _updatedLayers["data"][layerIndex]["layerColors"]
        layerColors[colorIndex] = {
            k: updatedColorList.map((c) => ({ k: c[0], v: c[1] })),
            v: colorHex
        }
    }
    return _updatedLayers["data"]
}