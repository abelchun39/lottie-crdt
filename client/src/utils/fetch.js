import axios from 'axios'

const axiosConfig = {
  timeout: 6000000
}

axiosConfig.baseURL = (process.env.REACT_APP_BASE_API) ? process.env.REACT_APP_BASE_API : "http://localhost:5000"

// Create Axios
const service = axios.create(axiosConfig)

// Request
service.interceptors.request.use(config => {
  return config
}, error => {
  // Do something with request error
  console.log(error) // for debug
  Promise.reject(error)
})

// Response
service.interceptors.response.use(
  response => response,
  error => {
    if (error.response) {
      console.error(error)
    }
    return Promise.reject(error)
  }
)

export default service
