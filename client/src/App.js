import React from 'react';
import { Provider } from "react-redux";
import { createMuiTheme, responsiveFontSizes, ThemeProvider } from '@material-ui/core/styles';
import { SnackbarContainer } from 'uno-material-ui';
import { useQueryParam } from 'use-query-params'
import GlobalStateProvider from "@morefaie/react-useglobalstate";
import shortid from 'shortid'
import { useStore } from "./store/useStore";
import AppBar from './components/AppBar'
import {LeftDrawer} from './components/LeftDrawer'
import {LottiePlayer} from './components/Player'
import {RightDrawer} from './components/RightDrawer'
import { initialLocalGlobalState } from './store/redux'

let theme = createMuiTheme({
  palette: {
    primary: {
      main: "#0fccce",
    },
    secondary: {
      main: '#414141',
    },
  },
});
theme = responsiveFontSizes(theme);

export const App = () => {
  const [room, setRoom] = useQueryParam('room');
  const generateNewRoom = () => {
      const newRoom = shortid.generate();
      setRoom(newRoom);
      return newRoom;
  };
  const appStore = useStore(room, generateNewRoom);
  return (
    appStore ? (
      <GlobalStateProvider state={initialLocalGlobalState}>
        <Provider store={appStore}>
          <ThemeProvider theme={theme}>
            <SnackbarContainer/>
            <React.Fragment>
              <AppBar/>
              <LeftDrawer/>
              <LottiePlayer/>
              <RightDrawer/>
            </React.Fragment>
          </ThemeProvider>
        </Provider>
      </GlobalStateProvider>) : null
  )
};
