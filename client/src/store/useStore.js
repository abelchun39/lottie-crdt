import { useEffect, useState } from 'react';
import { storeManager } from './redux';

export const useStore = (room, generateNewRoom) => {
    const [appStore, setAppStore] = useState(null);
    useEffect(() => {
        if (room) {
            storeManager.joinStore(room).then((newStore) => setAppStore(newStore));
        }
        else {
            const newRoom = generateNewRoom();
            storeManager.createStore(newRoom).then((newStore) => setAppStore(newStore));
        }
    }, [room]);
    return appStore;
};
