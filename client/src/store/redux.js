import { StoreManager } from '@localfirst/state';
import { proxyReducer } from './reducers';

/* Global Local States */
export const initialLocalGlobalState = {
  selectedLayerIndex: 0,
  lottie: {},
  layers: [],
  isLoading: true
}

/* Proxy Redux States (AutoMerge) */
const initialState = {
  actions: []
};

const urls = process.env.REACT_APP_RELAYS ? process.env.REACT_APP_RELAYS.split(',') : ["ws://localhost:8080"]

// const middlewares = [];
export const storeManager = new StoreManager({
    databaseName: 'lottieDB',
    collections: ['actions'],
    proxyReducer,
    initialState,
    urls
    // middlewares
});
