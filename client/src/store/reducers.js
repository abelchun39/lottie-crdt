import { ActionType } from './actions'
import _ from 'lodash'
import shortid from 'shortid'

const { MANAGE_ACTION } = ActionType

export const proxyReducer = (state, { type, payload }) => {
    switch (type) {
        case MANAGE_ACTION: {
            const { details } = payload;
            if(state && state["actions"] && Object.keys(state["actions"]).length > 0) {
                let actions = Object.values(state["actions"]);
                let found = _.findLast(actions, { layerIndex: details["layerIndex"], colorIndex: details["colorIndex"] })
                if (found) {
                    // Update existing action
                    return {
                        collection: 'actions',
                        id: found["id"],
                        fn: action => {
                          Object.assign(action, details)
                        }
                    }
                }
            }
            // Create new action
            let actionId = shortid.generate()
            details["id"] = actionId
            return {
                collection: 'actions',
                id: actionId,
                fn: action => {
                    Object.assign(action, details)
                }
            }
        }

        default:
            return null
    }
}
