export const ActionType = {
    MANAGE_ACTION: "MANAGE_ACTION"
}

export default {
    manageAction: details => ({
        type: ActionType.MANAGE_ACTION,
        payload: { details }
    })
}
