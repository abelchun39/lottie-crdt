import _ from 'lodash'

export const getActions = (state) => {
    if (!state || !state.actions || (state.actions && Object.keys(state.actions).length === 0))
        return {};
    let actionList = Object.values(state.actions)
    return actionList
}